package com.gofrugal.library.keyboard.listeners

interface OnKeyboardDoneButtonListener {
    fun onCurrencyKeyboardDone(): Boolean
}