package com.gofrugal.library.keyboard

import android.content.Context
import android.inputmethodservice.InputMethodService
import android.inputmethodservice.Keyboard
import android.inputmethodservice.KeyboardView
import android.view.View
import android.widget.EditText
import com.gofrugal.library.currencyedittext.R
import java.lang.String as JString

/**
 * Created by kumaran on 26/05/16.
 */
class CurrencyKeyboard(val context: Context, val view: EditText, val doneButtonHandler: () -> Boolean) : InputMethodService(), KeyboardView.OnKeyboardActionListener {
    private var keyboardView: KeyboardView? = null
    private var keyboard: Keyboard? = null

    override fun onRelease(primaryCode: Int) {
    }

    override fun onKey(primaryCode: Int, keyCodes: IntArray?) {
        val editable = view.text
        val start = view.selectionStart

        when (primaryCode) {
            Keyboard.KEYCODE_DELETE -> {
                if (editable != null && start > 0) editable.delete(start - 1, start);
            }
            55000 -> {
                if(editable.toString().equals("")) {
                    return
                }
                editable.insert(start, java.lang.String.valueOf("00"))
            }
            55001 -> {
                if(editable.contains(".")) return
                if(editable.length == 0) {
                    editable.insert(view.selectionStart, java.lang.String.valueOf("0"))
                }
                editable.insert(view.selectionStart, java.lang.String.valueOf("."))
            }
            Keyboard.KEYCODE_DONE -> {
                doneButtonHandler()
            }
            else -> {
                val code: Char = primaryCode.toChar()
                editable.insert(start, java.lang.String.valueOf(code))
            }
        }
    }

    override fun swipeRight() {
    }

    override fun swipeDown() {
    }

    override fun swipeLeft() {
    }

    override fun onPress(primaryCode: Int) {
    }

    override fun onText(text: CharSequence?) {
    }

    override fun swipeUp() {
    }

    override fun onCreateInputView(): View? {
        keyboardView = layoutInflater.inflate(R.layout.keyboard, null) as KeyboardView
        keyboard = Keyboard(this, R.xml.currency_keyboard)
        keyboardView!!.keyboard = keyboard
        keyboardView!!.setOnKeyboardActionListener(this)
        return keyboardView
    }

    fun keyBoard(): Keyboard {
        return Keyboard(context, R.xml.currency_keyboard)
    }
}


