package com.gofrugal.library.ui

import android.app.Activity
import android.content.Context
import android.inputmethodservice.KeyboardView
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.gofrugal.library.currencyedittext.R
import com.gofrugal.library.keyboard.CurrencyKeyboard
import com.gofrugal.library.keyboard.listeners.OnKeyboardDoneButtonListener
import com.gofrugal.library.util.CurrencyTextFormatter
import com.gofrugal.library.util.CurrencyTextWatcher
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

@SuppressWarnings("unused")
class CurrencyEditText(context: Context, attrs: AttributeSet) : EditText(context, attrs) {
    val ENGLISH = "en"
    val INDIA = "IN"

    var locale: Locale? = null

    var onKeyboardDoneButtonListener: OnKeyboardDoneButtonListener? = null

    val doneButtonHandler = {
        val actionFailure = onKeyboardDoneButtonListener!!.onCurrencyKeyboardDone()
        if(!actionFailure){
            hideCustomKeyboard()
        }else{
            true
        }
    }

    private var rawValue = 0L
    var keyboardView: KeyboardView? = null
        set (kv){
            field = kv
            val currencyKeyboard = CurrencyKeyboard(context, this, doneButtonHandler)
            this.keyboardView!!.keyboard = currencyKeyboard.keyBoard()
            this.keyboardView!!.isPreviewEnabled = false
            this.keyboardView!!.setOnKeyboardActionListener(currencyKeyboard)

            this.setOnFocusChangeListener { view, hasFocus ->
                (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, InputMethodManager.RESULT_HIDDEN, null)
                when {
                    hasFocus -> showCustomKeyboard(view)
                    else -> hideCustomKeyboard()
                }
            }

            this.setOnClickListener { view ->
                showCustomKeyboard(view)
            }
        }

    fun currencyValue(): Double {
         return (rawValue / Math.pow(10.0, 2.0));
    }

    fun formattedCurrencyValue(fractionDigits: Int): String {
        val currencyValue = currencyValue()
        val decimalFormat = DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH))
        decimalFormat.maximumFractionDigits = fractionDigits
        return decimalFormat.format(currencyValue)
    }


    init {
        processAttributes(context, attrs)
        this.inputType = 0x00;
        val currencyTextWatcher = CurrencyTextWatcher(this)
        this.addTextChangedListener(currencyTextWatcher)
    }

    fun formatCurrency(rawVal: Long): String {
        return CurrencyTextFormatter.formatText(rawVal.toString(), locale!!)
    }

    fun setValueInLowestDenominator(mValueInLowestDenominator: Long?) {
        this.rawValue = mValueInLowestDenominator!!
    }

    private fun processAttributes(context: Context, attrs: AttributeSet) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.CurrencyEditText)

        val languageCode = array.getString(R.styleable.CurrencyEditText_language_code) ?: ENGLISH
        val countryCode = array.getString(R.styleable.CurrencyEditText_country_code) ?: INDIA
        try {
            this.locale = Locale(languageCode.toLowerCase(), countryCode.toUpperCase())
        } catch (e: Exception) {
            this.locale = Locale(ENGLISH, INDIA)
        }
        array.recycle()
    }

    fun hideCustomKeyboard(): Boolean {
        this.keyboardView!!.visibility = View.GONE
        this.keyboardView!!.isEnabled = false
        return false
    }

    fun showCustomKeyboard(view: View) {
        (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS, null)
        this.keyboardView!!.visibility = View.VISIBLE
        this.keyboardView!!.isEnabled = true
    }

    fun isCustomKeyboardVisible(): Boolean {
        return (keyboardView!!.visibility == View.VISIBLE)
    }

}

