package com.gofrugal.library.ui

import android.app.Activity
import android.content.Context
import android.inputmethodservice.KeyboardView
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.gofrugal.library.currencyedittext.R
import com.gofrugal.library.keyboard.NumberKeyboard
import com.gofrugal.library.keyboard.listeners.OnKeyboardDoneButtonListener


@SuppressWarnings("unused")
class NumberEditText(context: Context, attrs: AttributeSet) : EditText(context, attrs) {
    val DEFAULT_DECIMAL_SEPARATOR = "."
    val DEFAULT_GROUPING_SEPARATOR = ","
    val DEFAULT_NO_OF_DECIMAL_PLACES: Int = 2

    private var groupingSeparator: String = DEFAULT_GROUPING_SEPARATOR
    private var decimalSeparator: String = DEFAULT_DECIMAL_SEPARATOR
    private var numberOfDecimalPlaces: Int = DEFAULT_NO_OF_DECIMAL_PLACES
    private var millionsSeparation: Boolean = false

    var onKeyboardDoneButtonListener: OnKeyboardDoneButtonListener? = null

    val doneButtonHandler = {
        onKeyboardDoneButtonListener!!.onCurrencyKeyboardDone()
        hideCustomKeyboard()
    }

    var keyboardView: KeyboardView? = null
        set (kv) {
            field = kv
            val numberKeyboard = NumberKeyboard(context, this, doneButtonHandler)
            this.keyboardView!!.keyboard = numberKeyboard.keyBoard()
            this.keyboardView!!.isPreviewEnabled = false
            this.keyboardView!!.setOnKeyboardActionListener(numberKeyboard)

            this.setOnFocusChangeListener { view, hasFocus ->
                (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, InputMethodManager.RESULT_HIDDEN, null)
                when {
                    hasFocus -> showCustomKeyboard(view)
                    else -> hideCustomKeyboard()
                }
            }

            this.setOnClickListener { view ->
                showCustomKeyboard(view)
            }
        }

    init {
        processAttributes(context, attrs)
        this.inputType = 0x00;
    }

    private fun processAttributes(context: Context, attrs: AttributeSet) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.NumberEditText)

        this.decimalSeparator = array.getString(R.styleable.NumberEditText_decimalSeparator) ?: DEFAULT_DECIMAL_SEPARATOR
        this.groupingSeparator = array.getString(R.styleable.NumberEditText_groupingSeparator) ?: DEFAULT_GROUPING_SEPARATOR
        this.numberOfDecimalPlaces = array.getInt(R.styleable.NumberEditText_numberOfDecimalPlaces, DEFAULT_NO_OF_DECIMAL_PLACES)
        this.millionsSeparation = array.getBoolean(R.styleable.NumberEditText_millionsSeparation, false)

        array.recycle()
    }

    fun hideCustomKeyboard(): Boolean {
        this.keyboardView!!.visibility = View.GONE
        this.keyboardView!!.isEnabled = false
        return false
    }

    fun showCustomKeyboard(view: View) {
        (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS, null)
        this.keyboardView!!.visibility = View.VISIBLE
        this.keyboardView!!.isEnabled = true
    }
}

