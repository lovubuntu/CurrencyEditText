package com.gofrugal.library.util

import android.text.Editable
import android.text.TextWatcher
import com.gofrugal.library.ui.CurrencyEditText

import java.text.DecimalFormat

@SuppressWarnings("unused")
internal class
CurrencyTextWatcher(private val editText: CurrencyEditText) : TextWatcher {

    private var ignoreIteration: Boolean = false
    private var lastGoodInput: String? = null
    private val currencyFormatter: DecimalFormat

    val CURRENCY_DECIMAL_DIVISOR: Double
    val CURSOR_SPACING_COMPENSATION = 2
    val MAX_RAW_INPUT_LENGTH = 15


    init {
        lastGoodInput = ""
        ignoreIteration = false

        currencyFormatter = DecimalFormat.getInstance(editText.locale) as DecimalFormat

        CURRENCY_DECIMAL_DIVISOR = Math.pow(10.0, 2.0)

    }

    override fun afterTextChanged(editable: Editable) {
        //Use the ignoreIteration flag to stop our edits to the text field from triggering an endlessly recursive call to afterTextChanged
        if (!ignoreIteration) {
            ignoreIteration = true
            //Start by converting the editable to something easier to work with, then remove all non-digit characters
            var newText = editable.toString()
            val textToDisplay: String

            newText = newText.replace("[^0-9]".toRegex(), "")

            if (newText != "" && newText.length < MAX_RAW_INPUT_LENGTH && newText != "-") {
                editText.setValueInLowestDenominator(java.lang.Long.valueOf(newText))
            }
            else if (newText == "") {
                editText.setValueInLowestDenominator(0)
            }
            try {
                textToDisplay = CurrencyTextFormatter.formatText(newText, editText.locale!!)
            } catch (exception: IllegalArgumentException) {
                textToDisplay = lastGoodInput!!
            }

            editText.setText(textToDisplay)
            lastGoodInput = textToDisplay

            var cursorPosition = editText.text.length
            if (textToDisplay.length > 0 && Character.isDigit(textToDisplay[0])) cursorPosition -= CURSOR_SPACING_COMPENSATION

            editText.setSelection(cursorPosition)

        } else {
            ignoreIteration = false
        }
    }


    override fun beforeTextChanged(charSequence: CharSequence, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
    }
}
