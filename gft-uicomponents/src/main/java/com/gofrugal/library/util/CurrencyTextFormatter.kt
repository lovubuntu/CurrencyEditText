package com.gofrugal.library.util

import java.text.DecimalFormat
import java.util.*

internal object CurrencyTextFormatter {
    internal val MAX_RAW_INPUT_LENGTH = 15

    fun formatText(value: String, locale: Locale): String {
        var mValue: String = value
        val CURRENCY_DECIMAL_DIVISOR = Math.pow(10.0, 2.0).toInt().toDouble()
        val currencyFormatter = DecimalFormat.getCurrencyInstance(locale) as DecimalFormat
//        currencyFormatter.currency = currency

        if (mValue != "" && mValue.length < MAX_RAW_INPUT_LENGTH && mValue != "-") {
            var newTextValue = java.lang.Double.valueOf(mValue)!!
            newTextValue /= CURRENCY_DECIMAL_DIVISOR
            mValue = currencyFormatter.format(newTextValue)
        } else if(mValue == ""){
            mValue = ""
        }
        else{
            throw IllegalArgumentException("Invalid amount of digits found (either zero or too many) in argument val")
        }
        return mValue
    }

}
