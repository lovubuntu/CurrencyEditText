package com.gofrugal.library.currencyedittexttester

import android.app.Activity
import android.inputmethodservice.KeyboardView
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.gofrugal.library.keyboard.listeners.OnKeyboardDoneButtonListener
import com.gofrugal.library.ui.CurrencyEditText


class MainActivity : Activity(), OnKeyboardDoneButtonListener {
    override fun onCurrencyKeyboardDone(): Boolean {
        if (currencyEditText!!.currencyValue() >  10){
            return false
        }
        else{
            Toast.makeText(this, "Please enter currency greater than 10", Toast.LENGTH_SHORT).show()
            return true
        }
    }


//    var cet: NumberEditText? = null
    var currencyEditText: CurrencyEditText? = null
    var keyboardView: KeyboardView? = null
    var valueText: TextView? = null
    var button: Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        cet = findViewById(R.id.cet) as NumberEditText?
        keyboardView = findViewById(R.id.keyboardview) as KeyboardView?
        valueText = findViewById(R.id.valueText) as TextView?
        currencyEditText = findViewById(R.id.currencyText) as CurrencyEditText?
        currencyEditText!!.keyboardView = keyboardView!!
        currencyEditText!!.onKeyboardDoneButtonListener = (this as OnKeyboardDoneButtonListener)
        button = findViewById(R.id.button) as Button?
//        cet!!.keyboardView = keyboardView!!
//        cet!!.onKeyboardDoneButtonListener = (this as OnKeyboardDoneButtonListener)
//        button!!.setOnClickListener {
//            valueText!!.text = cet!!.text
//        }
    }

    fun clearText(view: View) {
        Toast.makeText(this, "Clear clicked" + currencyEditText!!.currencyValue(), Toast.LENGTH_SHORT).show()
        currencyEditText!!.setText("")
    }
    fun showText(view: View) {
        Toast.makeText(this, "Clear clicked" + currencyEditText!!.currencyValue(), Toast.LENGTH_SHORT).show()
    }

    fun formattedCurrency(view: View) {
        Toast.makeText(this, "Formatted Value"+ currencyEditText!!.formattedCurrencyValue(2), Toast.LENGTH_SHORT).show()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}
